const functions = require('firebase-functions');

const SlackWebhook = require('slack-webhook');
const slack = new SlackWebhook('https://hooks.slack.com/services/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');

const Client = require('node-rest-client').Client;
const client = new Client();

exports.addToFollowing = functions.database.ref('/follow/{initiatorUid}/{interestedInFollowingUid}')
  .onCreate(event => {
  const initiatorUid = event.params.initiatorUid;
  const interestedInFollowingUid = event.params.interestedInFollowingUid;
  const rootRef = event.data.ref.root;
  let FollowingMeRef = rootRef.child('usersFollowingMe/' + interestedInFollowingUid + "/" + initiatorUid);
  return FollowingMeRef.set(true);
});

exports.notifyOfNewUser = functions.database.ref('/users/{userId}')
  .onCreate(event => {
    const newUser = event.data.val();
    console.log(newUser);
    slack.send(newUser);
  });

const sendLiveMessage = (messageToken, imageName) => {

  console.log('In send Live message', messageToken, imageName);

  const args = {
    headers: {
      "Content-Type": "application/json",
      "Authorization": "key=AAAAqRexHb0:APA91bG8sqVkXxcst2dqfXTbdSmY2SWL9JouetLCn5uImmDVFjvQrhBnGq-4qPJmaYQTxy7Nc42rNpDl-WaVMmNzFENWL6-p7fPIvzEHyFoR2F6CjackRnnPqkUIcM4ys5w504780OrE"
    },
    data: {
      to: messageToken,
      notification: {
        title: "Congratulations!!",
        body: `Your image ${imageName} has been favorited`
      }

    }
  };

  client.post("https://fcm.googleapis.com/fcm/send", args, (data, response) => {
    console.log(data);
    console.log(response);
  });


};


exports.notifyWhenImageIsFavorited = functions.database.ref('/images/{images}')
  .onUpdate(event => {

    const imageData = event.data.val();

    if (imageData.oldFavoriteCount < imageData.favoriteCount) {

      const uploadedBy = imageData.uploadedBy;
      const rootRef = event.data.ref.root;
      rootRef.child('/users/'+ uploadedBy.uid).once('value')
        .then(snapshot => {
          const user = snapshot.val();
          const messageToken = user.messageToken;
          sendLiveMessage(messageToken, imageData.name);

        });

      const imageRef = rootRef.child('/images/' + imageData.name + "/oldFavoriteCount");
      return imageRef.set(imageData.favoriteCount);
    }


  });
