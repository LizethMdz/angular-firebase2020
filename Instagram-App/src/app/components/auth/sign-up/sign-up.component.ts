
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private authS: AngularFireAuth,
              private notifier: NotificationService) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm){
    const fullname = form.value.fullname;
    const email = form.value.email;
    const password = form.value.password;

    this.authS.createUserWithEmailAndPassword(email, password)
      .then(userData => {
        console.log(userData);
        this.authS.currentUser.then( auth => {
          auth.sendEmailVerification();
          const message = `A verification email has been sent to ${email}. kindly check your inbox and follow the steps
          in the verification email. Once verification is complete, please login to the application`;
          this.notifier.display('success', message);

          return firebase.database().ref('users/' + auth.uid).set({
            email: email,
            uid: auth.uid,
            registrationDate: new Date().toString(),
            name: fullname
          })
          .then(() => {
            this.authS.signOut();
          });

        }).catch(error => {
          this.notifier.display('error', error.message);
        });
        
      })
      .catch(error => {
        this.notifier.display('error', error.message);
      });
  }

  // signUp(f: NgForm){
  //   const fullname = f.value.fullname;
  //   const email = f.value.email;
  //   const password = f.value.password;

  //   return new Promise((resolve, reject) => {
  //     this.authS.createUserWithEmailAndPassword(email, password)
  //     .then(datos => {
  //       console.log(datos);
  //       this.authS.currentUser.then( auth => {
  //         auth.sendEmailVerification();
  //         const message = `A verification email has been sent to ${email}. kindly check your inbox and follow the steps
  //         in the verification email. Once verification is complete, please login to the application`;
  //         this.notifier.display('success', message);

  //         return firebase.database().ref('users/' + auth.uid).set({
  //           email: email,
  //           uid: auth.uid,
  //           registrationDate: new Date().toString(),
  //           name: fullname
  //         })
  //         .then(() => {
  //           firebase.auth().signOut();
  //         });

  //       }).catch(error => {
  //         this.notifier.display('error', error.message);
  //       });
  //       resolve(datos)
  //     },
  //     error => {
  //       reject(this.notifier.display('error', error.message))
  //     })
  //   });
  // }

}
