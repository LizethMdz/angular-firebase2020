import { UserService } from './../../../shared/user.service';
import { MyfireService } from './../../../shared/myfire.service';
import { Component, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/shared/notification.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private notifier: NotificationService,
              private authS: AngularFireAuth,
              private router: Router,
              private myFireS: MyfireService,
              private userS: UserService) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm){
    const email = form.value.email;
    const password = form.value.password;

    this.authS.signInWithEmailAndPassword(email, password)
    .then(userData => {
      if (userData.user.emailVerified) {
        console.log('Verified!! :)');
        return this.myFireS.getUserFromDatabase(userData.user.uid);
      } else {
        const message = 'Your email is not yet verified';
        this.notifier.display('error', message);
        this.authS.signOut();
      }
    })
    .then(userDataFromDatabase => {
      if (userDataFromDatabase) {
        console.log(userDataFromDatabase);
        this.userS.set(userDataFromDatabase);
        this.router.navigate(['/allposts']);
      }
    })
    .catch(err => {
      this.notifier.display('error', err.message);
    });

  }

}
