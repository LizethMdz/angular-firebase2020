import { AngularFireAuth } from '@angular/fire/auth';
import { MyfireService } from './../../shared/myfire.service';
import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.css']
})
export class MyPostsComponent implements OnInit {
  personalPostsRef: any;
  /**Recibe la informacion de la base de datos */
  postLists: any = [];
  constructor(private myFire: MyfireService, 
              private notifier: NotificationService,
              private authS: AngularFireAuth) { }

  ngOnInit(): void {
    this.authS.currentUser.then( auth => {
      console.log(auth.uid);
      const uid = auth.uid;
      this.personalPostsRef = this.myFire.getUserPostsRef(uid);
      this.personalPostsRef.on('child_added', data => {
        this.postLists.push({
          key: data.key,
          data: data.val()
        });
      });
    });
    
  }

  onFileSelection(event) {
    const fileList: FileList = event.target.files;

    if (fileList.length > 0) {
      const file: File = fileList[0];
      this.myFire.uploadFile(file)
        .then(data => {
          this.notifier.display('success', 'Picture Successfully uploaded!!');
          return this.myFire.handleImageUpload(data);
        })
        .catch(err => {
          this.notifier.display('error', err.message);
          console.log(err);
        });
    }
  }

}
