import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: boolean = false;
  name: string;
  uid: string;
  email: string;
  constructor(private authS: AngularFireAuth,
    private userS: UserService,
    private router: Router) { }

  ngOnInit(): void {
    this.userS.statusChange.subscribe(userData => {
      if (userData) {
        this.name = userData.name;
        this.email = userData.email;
        this.uid = userData.uid;
      } else {
        this.name = null;
        this.email = null;
        this.uid = null;
      }
    });

    this.authS.onAuthStateChanged(userData => {
      // we are logged in
      if (userData && userData.emailVerified) {
        this.isLoggedIn = true;
        const user = this.userS.getProfile();
        if (user && user.name) {
          this.name = user.name;
          this.email = user.email;
          this.uid = user.uid;
        }
        this.router.navigate(["/allposts"]);
      } else {
        this.isLoggedIn = false;
      }
    });
  }

  onLogout() {
    this.authS.signOut()
      .then(() => {
        this.userS.destroy();
        this.isLoggedIn = false;
      });
  }

}
