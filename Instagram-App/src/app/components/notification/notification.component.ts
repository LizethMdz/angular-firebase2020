import { NotificationService } from './../../shared/notification.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  type: string = null;
  message: string = null;
  constructor(private notifier: NotificationService) {
    /**Cargar la notificacion */
    notifier.emmitter.subscribe(
      data => {
        this.type = data.type;
        this.message = data.message;
        this.reset();
      }
    );
  }
  /**Duracion de la notificacion */
  reset() {
    setTimeout(() => {
      this.type = null;
      this.message = null;
    }, 6000);
  }

  ngOnInit(): void {
  }

}
