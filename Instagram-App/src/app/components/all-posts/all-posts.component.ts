import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/shared/notification.service';
import { MyfireService } from 'src/app/shared/myfire.service';
import * as firebase from 'firebase';
@Component({
  selector: 'app-all-posts',
  templateUrl: './all-posts.component.html',
  styleUrls: ['./all-posts.component.css']
})
export class AllPostsComponent implements OnInit {
  allRef: any;
  loadMoreRef: any;
  all: any = [];
  constructor(private myFire: MyfireService, 
    private notifier: NotificationService) { }

  ngOnInit(): void {
    this.allRef = firebase.database().ref('allposts').limitToFirst(3);
    this.allRef.on('child_added', data => {
      this.all.push({
        key: data.key,
        data: data.val()
      });
    });
    console.log(this.all);
  }

  onLoadMore() {
    if (this.all.length > 0) {
      const lastLoadedPost = this.all[this.all.length - 1];
      const lastLoadedPostKey = lastLoadedPost.key;

      this.loadMoreRef = firebase.database().ref('allposts').startAt(null, lastLoadedPostKey).limitToFirst(3 + 1);

      this.loadMoreRef.on('child_added', data => {

        if (data.key === lastLoadedPostKey) {
          return;
        } else {
          this.all.push({
            key: data.key,
            data: data.val()
          });
        }

      });

    }

  }

  ngOnDestroy() {
    this.allRef.off();
    if (this.loadMoreRef) {
      this.loadMoreRef.off();
    }
  }

  onFavoritesClicked(imageData) {
    this.myFire.handleFavoriteClicked(imageData)
      .then(data => {
        this.notifier.display('success', 'Image added to favorites');
      })
      .catch(err => {
        this.notifier.display('error', 'Error adding image to favorites');
      });
  }

  onFollowClicked(imageData) {
    this.myFire.followUser(imageData.uploadedBy)
      .then(() => {
        this.notifier.display('success', 'Following ' + imageData.uploadedBy.name + "!!!");
      })
      .catch(err => {
        this.notifier.display('error', err);
      });

  }

}
