import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  /**Obtiene la información de la base de datos, para que la pueda procesar */
  @Input() imageName: string;
  @Input() displayPostedBy: boolean = true;
  @Input() displayFavoritesButton: boolean = true;
  @Input() displayFollowButton: boolean = true;
  defaultImage: string = "http://via.placeholder.com/150x150";
  imageData: any = {};

  /**Variables a exportar para saber si fueron elegidas como
   * favoritas o seguidas por alguien.
   */
  @Output() favoriteClicked = new EventEmitter<any>();
  @Output() followClicked = new EventEmitter<any>();

  constructor(private authS: AngularFireAuth) { }

  ngOnInit(): void {
    
    this.authS.currentUser.then( userData => {
      const uid = userData.uid;

      firebase.database().ref('images').child(this.imageName)
        .once('value')
        .then(snapshot => {
          this.imageData = snapshot.val();
          this.defaultImage = this.imageData.fileUrl;
  
          if (this.imageData.uploadedBy.uid === uid) {
            this.displayFavoritesButton = false;
            this.displayFollowButton = false;
          }
        });
    });
    
  }

  onFavoritesClicked() {
    this.favoriteClicked.emit(this.imageData);
  }

  onFollowClicked() {
    this.followClicked.emit(this.imageData);
  }

}
