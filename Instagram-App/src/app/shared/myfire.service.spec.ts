import { TestBed } from '@angular/core/testing';

import { MyfireService } from './myfire.service';

describe('MyfireService', () => {
  let service: MyfireService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyfireService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
