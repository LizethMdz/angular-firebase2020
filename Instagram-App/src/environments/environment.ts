// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAUKQ0zw-No-Whk8AcPX48fQyAdDkAF-co",
    authDomain: "instagram-angular-cdee1.firebaseapp.com",
    databaseURL: "https://instagram-angular-cdee1.firebaseio.com",
    projectId: "instagram-angular-cdee1",
    storageBucket: "instagram-angular-cdee1.appspot.com",
    messagingSenderId: "947551378439",
    appId: "1:947551378439:web:5cc4ff0749eca3b0725ea2",
    measurementId: "G-8DR7VZ8F7K"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
